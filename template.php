<?php
/*
  Preprocess
*/ 



function bucket_preprocess_html(&$vars) {
	$vars['head_title'] = strip_tags(html_entity_decode($vars['head_title']));
	
	// dpm($vars);
 
  if (isset($vars['page']['sidebar_first']['system_main-menu'])) {
     $vars['classes_array'][] = 'sidebar-main-menu';
  }
  
}

function bucket_preprocess_page(&$variables) {
	// add Brown's web fonts
	drupal_add_css('//cloud.typography.com/793622/644668/css/fonts.css','external');
	// expose the site slogan
	$variables['site_slogan'] = variable_get('site_slogan', '');
	// banner
	
	// do we have a custom robust banner?
	$path = theme_get_setting('robust_banner_image_path');
	$height = theme_get_setting('robust_banner_image_height');
	if ( !empty( $path ) && !empty( $height ) ) {
		$variables['robust_banner_image_path'] = file_create_url( $path );
		$variables['robust_banner_image_height'] = $height;
	}
	else {
	// default theme robust banner
//		$vars['robust_banner_image_path'] = base_path() . PAWTUCKET_PATH . '/img/sample-large-banner.jpg';
//		$vars['robust_banner_image_height'] = 360;
	}



}

 
 /*
   Process
 */
function bucket_process_html(&$vars) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

function bucket_process_page(&$vars) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}




function bucket_breadcrumb($variables) {
	// if breadcrumbs are off, return an empty string
	if(!theme_get_setting('mothership_show_breadcrumbs','bucket')) {
		return '';
	}
	$breadcrumb_string = '<div class="breadcrumbs">';
	$home = t('Home');
	$separator = ' <span class="separator">' . theme_get_setting('mothership_breadcrumb_separator','bucket') . '</span> ';
	if (count($variables['breadcrumb']) > 0) {
		// check if the settings say to display current page title
		if(theme_get_setting('mothership_breadcrumb_current_page_title','bucket')) {
			$variables['breadcrumb'][] = drupal_get_title();
		}
		$breadcrumb_string .= implode($separator, $variables['breadcrumb']);
	} else {
		// if we want to show breadcrumbs on front page, this is where to do it:
		// $breadcrumb_string = $home;
	}
	$breadcrumb_string .= '</div>';
	
	// replace "home" with the preferred string
	$root_name = theme_get_setting('mothership_breadcrumb_home_text','bucket');
	if(empty($root_name)) {
		$root_name = variable_get('site_name', $home);
	}
	$breadcrumb_string = str_replace($home, $root_name, $breadcrumb_string);
	
	return $breadcrumb_string;

 }




/*
function bucket_preprocess_region(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/
/*
function bucket_preprocess_block(&$vars, $hook) {
  //  kpr($vars['content']);

  //lets look for unique block in a region $region-$blockcreator-$delta
   $block =  
   $vars['elements']['#block']->region .'-'. 
   $vars['elements']['#block']->module .'-'. 
   $vars['elements']['#block']->delta;
   
  // print $block .' ';
   switch ($block) {
     case 'header-menu_block-2':
       $vars['classes_array'][] = '';
       break;
     case 'sidebar-system-navigation':
       $vars['classes_array'][] = '';
       break;
    default:
    
    break;

   }


  switch ($vars['elements']['#block']->region) {
    case 'header':
      $vars['classes_array'][] = '';
      break;
    case 'sidebar':
      $vars['classes_array'][] = '';
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }

}
*/
/*
function bucket_preprocess_node(&$vars,$hook) {
  //  kpr($vars['content']);

  // add a nodeblock 
  // in .info define a region : regions[block_in_a_node] = block_in_a_node 
  // in node.tpl  <?php if($noderegion){ ?> <?php print render($noderegion); ?><?php } ?>
  //$vars['block_in_a_node'] = block_get_blocks_by_region('block_in_a_node');
}
*/
/*
function bucket_preprocess_comment(&$vars,$hook) {
  //  kpr($vars['content']);
}
*/
/*
function bucket_preprocess_field(&$vars,$hook) {
  //  kpr($vars['content']);
  //add class to a specific field
  switch ($vars['element']['#field_name']) {
    case 'field_ROCK':
      $vars['classes_array'][] = 'classname1';
    case 'field_ROLL':
      $vars['classes_array'][] = 'classname1';
      $vars['classes_array'][] = 'classname2';
      $vars['classes_array'][] = 'classname1';
    case 'field_FOO':
      $vars['classes_array'][] = 'classname1';
    case 'field_BAR':
      $vars['classes_array'][] = 'classname1';    
    default:
      break;
  }

}
*/
/*
function bucket_preprocess_maintenance_page(){
  //  kpr($vars['content']);
}
*/
/*
function bucket_form_alter(&$form, &$form_state, $form_id) {
  //if ($form_id == '') {
  //....
  //}
}
*/
