<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
//Bucket

$site_name =  variable_get('site_name');

if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- page.tpl.php-->
<?php } ?>
<?php print $mothership_poorthemers_helper; ?>

<div id="shell">
 

  <main>
    <div class="marginator">

          <div class="above-content">
            <?php print render($page['above_content']); ?>
          </div>
 
 
      <div role="main" id="content">
        <section id="main-content">
         <header id="content-header"> 
        <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
        <?php endif; ?>
      </header>
          <?php if ($action_links): ?>
          <ul class="action-links">
            <?php print render($action_links); ?>
          </ul>
          <?php endif; ?>
          <?php print render($title_prefix); ?> <?php print render($title_suffix); ?>
          <?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
          <nav class="tabs"><?php print render($tabs); ?></nav>
          <?php endif; ?>
          <?php if($page['highlighted'] OR $messages){ ?>
          <div class="drupal-messages"> <?php print render($page['highlighted']); ?> <?php print $messages; ?> </div>
          <?php } ?>
          
   
         	<div class="preface">
         		<?php print render($page['content_preface']); ?>
         	</div>
      
         
          <?php print render($page['content']); ?>
          
         
         	<div class="postscript">
         		<?php print render($page['content_postscript']); ?>
         	</div>
        
         
          </section>
        <?php if ($page['sidebar_second']): ?>
        <aside class="sidebar-second"> <?php print render($page['sidebar_second']); ?> </aside>
        <?php endif; ?>
      </div>
      <?php if ($page['sidebar_first']): ?>
      <div class="sidebar-first" id="sub-nav">
      <?php print render($page['sidebar_first']); ?> 
      </div>
      <?php endif; ?>
    </div>
  </main>
  <?php if ($page['main_bottom']): ?>
  <section id="main-bottom">
  	<?php print render($page['main_bottom']); ?>
  </section>
  <?php endif; ?>
  
  
  <section id="brown-nav">
    <div class="marginator">
    
      <div id="brown-header">
        <h1><a href="//www.brown.edu">Explore Brown University</a></h1>
        <span class="search-brown"><a href="<?php print $GLOBALS['base_url'] ?>/search/brown" id="search-brown-button">Search Brown</a></span> 
         <span class="toggle-brown"><a href="#" id="brown-menu-button" onclick="ga('send','event','Brown Menu','Click','Click Brown Hamburger')">Show/Hide</a></span> 
        </div>
        
      <nav id="u-nav">
        <div class="item-list" id="learn-list">
          <h3>Learn More</h3> 
          <ul>
            <li><a href="//www.brown.edu/about">About Brown</a></li>
            <li><a href="//www.brown.edu/academics">Academics</a></li>
            <li><a href="//www.brown.edu/admission">Admission</a></li>
            <li><a href="//www.brown.edu/research">Research</a></li>
            <li class="last"><a href="//www.brown.edu/campus-life">Campus Life</a></li>
          </ul>
        </div>
        <div class="item-list" id="find-list">
          <h3>Find</h3>
          <ul>
            <li class="first"><a href="//www.brown.edu/a-to-z">A to Z Index</a></li>
            <li class="last"><a href="//directory.brown.edu">People Directory</a></li>
          </ul>
        </div>
        <div class="item-list" id="information-list">
          <h3>Information for</h3>
          <ul>
            <li class="first"><a href="//www.brown.edu/gateway/current-students">Students</a></li>
            <li><a href="//www.brown.edu/gateway/faculty">Faculty</a></li>
            <li><a href="//www.brown.edu/gateway/staff">Staff</a></li>
            <li><a href="//www.brown.edu/gateway/families">Families</a></li>
            <li><a href="//www.brown.edu/gateway/alumni">Alumni</a></li>
            <li class="last"><a href="//www.brown.edu/gateway/friends-and-neighbors">Friends &amp; Neighbors</a></li>
          </ul>
        </div>
        <div class="item-list" id="destination-list">
          <h3>Top Destinations</h3>
          <ul>
            <li><a href="http://www.brown.edu/about/administration/global-engagement/">Global Brown</a></li>
            <li><a href="http://watson.brown.edu/">Watson Institute</a></li>
            <li><a href="http://www.brown.edu/gradschool">Graduate School</a></li>
            <li><a href="http://med.brown.edu">Alpert Medical School</a></li>
            <li><a href="http://www.brown.edu/academics/public-health/">School of Public Health</a></li>
            <li><a href="http://brown.edu/academics/engineering/">School of Engineering</a></li>
          </ul>
        </div>
      </nav>
      

 
  
   
            
  
      
      <?php if ($page['brown_nav']): ?>
      	<div id="brown-nav-region">
      		<?php print render($page['brown_nav']); ?>
      	</div>
      <?php endif; ?>
      
    </div>
  </section>
  

  
  <footer id="brown-footer">
    <div class="marginator">
      <section>
        <?php if ($page['brown_footer']): ?>
        <?php print render($page['brown_footer']); ?>
        <?php endif; ?>
        <div id="give-to-brown">
       	 <a class="giving-footer" href="http://giving.brown.edu">Giving to Brown</a>
        </div>
      </section>
      <div id="brown-contact">
        <h1>Brown University</h1>
        <p><span class="city-state-country">Providence, Rhode Island 02912, USA</span><br>
          <span class="telephone">Phone: 401-863-1000</span><br>
          <span class="maps-contact"><a href="http://www.brown.edu/Facilities/Facilities_Management/maps/#building/" tabindex="220">Maps</a> &amp; <a href="http://www.brown.edu/about/visit/driving-directions" tabindex="221">Directions</a> / <a href="http://www.brown.edu/contact" accesskey="9" tabindex="222">Contact Us</a></span><br>
          <span class="copyright">© <?php echo date('Y'); ?> Brown University</span></p>
        <p class="social-links">Connect: <a href="http://www.brown.edu/about/social-media/" title="Get Connected to the Brown Community">Social@Brown</a></p>
        <ul class="social-media">
          <li><a href="https://www.facebook.com/BrownUniversity" title="Facebook" id="footer-facebook">Facebook</a></li>
          <li><a href="https://twitter.com/BrownUniversity" id="footer-twitter">Twitter</a></li>
          <li><a href="http://www.youtube.com/brownuniversity" id="footer-youtube">YouTube</a></li>
          <li><a href="http://instagram.com/brownu" id="footer-instagram">Instagram</a></li>
          <li><a href="http://itunes.apple.com/institution/brown-university/id381076688#ls=1"  id="footer-itunesu">iTunes U</a></li>
          <li><a href="https://plus.google.com/100931085598361161047/posts" id="footer-google-plus">Google+</a></li>
          <li><a href="https://www.linkedin.com/edu/school?id=19348&amp;trk=tyah" id="footer-linkedin">LinkedIn</a></li>
          <li><a href="https://www.snapchat.com/add/brown-u" id="footer-snapchat">SnapChat</a></li>
        </ul>
      </div>
    </div>
  </footer>
  
</div>
</body>
</html>
