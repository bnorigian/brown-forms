<?php

function bucket_form_system_theme_settings_alter(&$form, $form_state) {
	$form['bucket_breadcrumb_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('&#8594; Breadcrumb Settings'),
		'#weight' => -50,
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['bucket_breadcrumb_settings']['mothership_show_breadcrumbs'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Show breadcrumbs'),
		'#default_value' => theme_get_setting('mothership_show_breadcrumbs'),
		'#description'   => t('Controls visibility of breadcrumb links.'),
	);
	$form['bucket_breadcrumb_settings']['mothership_breadcrumb_home_text'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Home text'),
		'#default_value' => theme_get_setting('mothership_breadcrumb_home_text'),
		'#description'   => t('Sets the text for the first breadcrumb.  The default is <em>Home</em>.  Leave this blank to use the site\'s title: <em>' . variable_get('site_name', '') . '</em>'),
	);
	$form['bucket_breadcrumb_settings']['mothership_breadcrumb_separator'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Separator'),
		'#default_value' => theme_get_setting('mothership_breadcrumb_separator'),
		'#description'   => t('Sets the separator for breadcrumbs.  The default is &rsaquo; <em>&amp;rsaquo;</em>.  Other commons symbols are: &#8594; <em>&amp;#8594;</em> and &#9658; <em>&amp;#9658;</em>'),
	);
	$form['bucket_breadcrumb_settings']['mothership_breadcrumb_current_page_title'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Show Current Page Title'),
		'#default_value' => theme_get_setting('mothership_breadcrumb_current_page_title'),
		'#description'   => t('Checking this option displays the current page title at the end of the breadcrumbs.'),
	);

	$form['bucket_banner_settings'] = array(
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#type' => 'fieldset',
		'#title' => t('&#8594; Banner Images'),
		'#description' => t("Upload custom banners for your site"),
		'#weight' => -48,
	);


	$robust_banner_image_path = $robust_banner_image_path_orig = theme_get_setting('robust_banner_image_path');
	// If $robust_banner_path is a public:// URI, display the path relative to the files
	// directory; stream wrappers are not end-user friendly.
	if (file_uri_scheme($robust_banner_image_path) == 'public') {
		//$robust_banner_image_path = file_uri_target($robust_banner_image_path);
	}
// make this textfield
	$form['bucket_banner_settings']['robust_banner_image_path'] = array(
		'#type' => 'hidden',
		'#title' => t('Path to robust banner'),
		'#default_value' =>  $robust_banner_image_path,
	);

	$form['bucket_banner_settings']['robust_banner_image_height'] = array(
		'#type' => 'hidden',
		'#title' => t('Height of robust banner'),
		'#default_value' => theme_get_setting('robust_banner_image_height'),
	);


	if (!empty( $robust_banner_image_path ) ) {
		$form['bucket_banner_settings']['robust_banner_image_preview'] = array(
			'#type' => 'item',
			'#title'    => t('Robust Banner image preview'),
			'#markup' => '<img src="' . file_create_url( $robust_banner_image_path ) . '?time=' . microtime(TRUE) . '">',
		);
	}


	$form['bucket_banner_settings']['robust_banner_image']=array(
		'#type'     => 'file',
		'#title'    => t('Robust Banner image'),
		'#description' => 'description',
		'#default_value' => theme_get_setting('robust_banner_image'), 
		'#upload_validators' => array(
			'file_validate_extensions' => array('gif png jpg jpeg'),
		),
	);

	$form['#submit'][] = 'bucket_settings_form_submit';

}


function bucket_settings_form_submit(&$form, &$form_state) {
	$settings = array();

	// Check for a new uploaded file, and use that if available.
	if ($file = file_save_upload('robust_banner_image')) {
		$parts = pathinfo($file->filename);
		//watchdog('theme', print_r($parts,true));
		$destination = 'public://' . 'bucket_robust_banner_image.' . $parts['extension'];
		$file->status = FILE_STATUS_PERMANENT;
		
		if ($newfile = file_copy($file, $destination, FILE_EXISTS_REPLACE)) {
			$_POST['robust_banner_image_path'] = $form_state['values']['robust_banner_image_path'] = $destination;
			
			$image_info = getimagesize($newfile->uri);
			//watchdog('theme', "image" . print_r($image_info,true));
			$form_state['values']['robust_banner_image_height'] = $image_info[1];
			
			file_usage_add($newfile, 'mantis', 'user', 1);
			file_delete($file); // delete original
		}
	}

	//cache_clear_all();
}