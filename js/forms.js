// JQuery
(function ($) {
	$(document).ready(function(e) {
		// alert("woot");
		// Show/Hide Brown Menu
		$("a#brown-menu-button").bind("click keypress", function(){		
			$("#brown-nav").toggleClass("show");
			return false;
		});
		// Show/Hide Site Menu
		$("a#site-menu-button").bind("click keypress", function(){		
			$("#site-nav, #banner").toggleClass("show");
			return false;
		});
		// Mobile Site Nav Show/Hide
		$("#also-in-site").bind("click keypress", function(){
			$(".block-system-main-menu").toggleClass("pop");
			return false;
		});
		// Desktop Search Show/Hide
		$("#search-button").bind("click keypress", function(){
			$(".block-search-form").toggleClass("out");
			return false;
		});


		// sticky class toggler for the nav for mobile widths so that it doesn't 
		// overlap the admin nav bar
		var nav = $('#site-nav');
		var className = 'not-scrolled';
		nav.addClass(className);
		$(window).scroll(function () {
			if ($(window).scrollTop() > 38) {
				nav.removeClass(className);
			} else {
				nav.addClass(className);
			}
		});


  
  
  });

$( "main article h6" ).wrapInner( "<span></span>" );

$( "#main-bottom h2" ).wrapInner( "<span></span>" );

$( ".page-people main h1" ).wrapInner( "<span></span>" );



})(jQuery);