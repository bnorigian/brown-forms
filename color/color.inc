<?php

$info = array(

// $black: #231F20;
// $brown: #3a1e1a;
// $beige: #f3f0ea; 
// $sand: #aba698;
// $primary: #513f3c; 
// $red: #C00404; 
// $yellow: #FFCC00; 


  // Available colors and color labels used in theme.
  'fields' => array(
    'base' => t('Base'),
    'tint' => t('Tint'),
    'tint2' => t('Lighter Tint'),
    'background' => t('Background'),
    'text' => t('Text'),
    'highlight' => t('Highlight'),
    'link' => t('Links'),
    'hover' => t('Link Hover'),
    'visited' => t('Link Visited'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Brown'),
      'colors' => array(
        'base' => '#3a1e1a',
        'tint' => '#aca698',
        'tint2' => '#efece5',
        'background' => '#ffffff',
        'text' => '#2f2a20',
        'highlight' => '#ffcc00',
        'link' => '#c00404',
        'hover' => '#c00404',
        'visited' => '#841616',
      ),
    ),
    'ams' => array(
      'title' => t('AMS'),
      'colors' => array(
        'base' => '#007da3',
        'tint' => '#aca698',
        'tint2' => '#efece5',
        'background' => '#ffffff',
        'text' => '#2f2a20',
        'highlight' => '#ffcc00',
        'link' => '#c00404',
        'hover' => '#c00404',
        'visited' => '#841616',

      ),
    ),
    'engineering' => array(
      'title' => t('Engineering'),
      'colors' => array(
        'base' => '#565F84',
        'tint' => '#aca698',
        'tint2' => '#efece5',
        'background' => '#ffffff',
        'text' => '#2f2a20',
        'highlight' => '#ffcc00',
        'link' => '#c00404',
        'hover' => '#c00404',
        'visited' => '#841616',
      ),
    ),

  ),

  // Images to copy over.
  'copy' => array(
//     'images/menu-collapsed.gif',
//     'images/menu-collapsed-rtl.gif',
//     'images/menu-expanded.gif',
//     'images/menu-leaf.gif',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/theme.css',
  ),

  // Gradient definitions.
  'gradients' => array(
//     array(
//       // (x, y, width, height).
//       'dimension' => array(0, 38, 760, 121),
//       // Direction of gradient ('vertical' or 'horizontal').
//       'direction' => 'vertical',
//       // Keys of colors to use for the gradient.
//       'colors' => array('top', 'bottom'),
//     ),
  ),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 760, 568),
    'link' => array(107, 533, 41, 23),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/body.png'                      => array(0, 37, 1, 280),
    'images/bg-bar.png'                    => array(202, 530, 76, 14),
    'images/bg-bar-white.png'              => array(202, 506, 76, 14),
    'images/bg-tab.png'                    => array(107, 533, 41, 23),
    'images/bg-navigation.png'             => array(0, 0, 7, 37),
    'images/bg-content-left.png'           => array(40, 117, 50, 352),
    'images/bg-content-right.png'          => array(510, 117, 50, 352),
    'images/bg-content.png'                => array(299, 117, 7, 200),
    'images/bg-navigation-item.png'        => array(32, 37, 17, 12),
    'images/bg-navigation-item-hover.png'  => array(54, 37, 17, 12),
    'images/gradient-inner.png'            => array(646, 307, 112, 42),

    'logo.png'                             => array(622, 51, 64, 73),
    'screenshot.png'                       => array(0, 37, 400, 240),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
